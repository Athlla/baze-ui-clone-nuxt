export const colors = {
	textColor: [
		{
			class: 'text-blue',
			text: 'Text class color = ".text-blue"'
		},
		{
			class: 'text-dark-grey',
			text: 'Text class color = ".text-dark-grey"'
		},
		{
			class: 'text-yellow',
			text: 'Text class color = ".text-yellow"'
		},
		{
			class: 'text-red',
			text: 'Text class color = ".text-red"'
		},
		{
			class: 'text-black',
			text: 'Text class color = ".text-black"'
		},
		{
			class: 'text-light-grey',
			text: 'Text class color = ".text-light-grey"'
		},
		{
			class: 'text-white',
			text: 'Text class color = ".text-white"'
		}
	],
	bgColor: [
		{
			class: 'bg-blue',
			text: 'Background class color = ".bg-blue"'
		},
		{
			class: 'bg-dark-grey',
			text: 'Background class color = ".bg-dark-grey"'
		},
		{
			class: 'bg-yellow',
			text: 'Background class color = ".bg-yellow"'
		},
		{
			class: 'bg-red',
			text: 'Background class color = ".bg-red"'
		},
		{
			class: 'bg-black',
			text: 'Background class color = ".bg-black"'
		},
		{
			class: 'bg-light-grey',
			text: 'Background class color = ".bg-light-grey"'
		},
		{
			class: 'bg-white',
			text: 'Background class color = ".bg-white"'
		}
	],
	blackTones: [
		{
			class: 'text-black10',
			text: 'Black tone = ".text-black10" ($black10)'
		},
		{
			class: 'text-black20',
			text: 'Black tone = ".text-black20" ($black20)'
		},
		{
			class: 'text-black30',
			text: 'Black tone = ".text-black30" ($black30)'
		},
		{
			class: 'text-black40',
			text: 'Black tone = ".text-black40" ($black40)'
		},
		{
			class: 'text-black50',
			text: 'Black tone = ".text-black50" ($black50)'
		},
		{
			class: 'text-black60',
			text: 'Black tone = ".text-black60" ($black60)'
		},
		{
			class: 'text-black70',
			text: 'Black tone = ".text-black70" ($black70)'
		},
		{
			class: 'text-black80',
			text: 'Black tone = ".text-black80" ($black80)'
		},
		{
			class: 'text-black90',
			text: 'Black tone = ".text-black90" ($black90)'
		},
		{
			class: 'text-black',
			text: 'Black tone = ".text-black" ($black)'
		}
	],
	linkColor: [
		{
			class: 'link-primary',
			text: 'Link color = ".link-primary" (Default link color)'
		},
		{
			class: 'link-dark-grey',
			text: 'Link color = ".link-dark-grey"'
		},
		{
			class: 'link-yellow',
			text: 'Link color = ".link-yellow"'
		},
		{
			class: 'link-red',
			text: 'Link color = ".link-red"'
		},
		{
			class: 'link-black',
			text: 'Link color = ".link-black"'
		},
		{
			class: 'link-light-grey',
			text: 'Link color = ".link-light-grey"'
		},
		{
			class: 'link-white',
			text: 'Link color = ".link-white"'
		}
	]
}
